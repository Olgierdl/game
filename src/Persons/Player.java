package Persons;


import Items.Armor;
import Items.Weapon;

public abstract class Player extends Humanoid
{
    public Player(int hp, int basicArmor, int attack, Weapon weapon, Armor armorItem)
    {
        super(hp, basicArmor, attack, weapon, armorItem);
        //super.setWeaponItem(weapon);
    }

//    public void attack(Person enemy) {
//        super.attack(enemy);
//    }

//    public Weapon getWeapon()
//    {
//        return weapon;
//    }
}
