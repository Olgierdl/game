package Persons.Enemies;

import Items.Item;
import Persons.Enemy;
import Persons.Person;

public class Goblin extends Enemy
{
    private static final int HP = 500;
    private static final int ARMOR = 2;
    private static final int ATTACK = 100;
    private static  final int ARMOR_PENETRATION = 5;

    public Goblin( Item item)
    {
        super(HP, ARMOR, ATTACK, item);
    }

    public void attack(Person enemy)
    {
        super.attack(enemy);
    }
}
