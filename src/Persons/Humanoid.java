package Persons;

import Items.Armor;
import Items.Weapon;

public abstract class Humanoid extends Person
{
    private Weapon weaponItem;
    private Armor armorItem;

    public Humanoid(int hp, int armor, int attack, Weapon weaponItem, Armor armorItem) {
        super(hp, armor, attack);
        this.weaponItem = weaponItem;
        this.armorItem = armorItem;
    }

    @Override
    public void attack(Person enemy)
    {
        enemy.getHurt(super.getAttack()+weaponItem.getAttack(), weaponItem.getArmorPenetration());
    }

//    public void setWeaponItem(Weapon weaponItem) {
//        this.weaponItem = weaponItem;
//    }
}
