package Persons;

import Items.Item;

public abstract class Enemy extends Person
{
    private Item item;

    public Enemy(int hp, int armor, int attack, Item item) {
        super(hp, armor, attack);
        this.item = item;
    }

}
