package Persons;

import Items.Armor;
import Items.Weapon;

public abstract class HumanoidEnemy extends Humanoid
{
    public HumanoidEnemy(int hp, int armor, int attack, Weapon weaponItem, Armor armorItem) {
        super(hp, armor, attack, weaponItem, armorItem);
    }
}
