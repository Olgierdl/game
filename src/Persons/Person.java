package Persons;

import Items.Armor;
import Items.Item;
import Items.Weapon;

public abstract class Person {
    private String name = "Test";
    private int hp;
    private int armor;
    private int attack;
    private int armorPenetration;

    public Person(int hp, int armor, int attack)
    {
        this.hp = hp;
        this.armor = armor;
        this.attack = attack;
    }


    public void getHurt(int amount, int armorPenetration)
    {
        hp -= damageValue(amount, armorPenetration); //odejmuje z życia wartość obliczoną przez damageValue()
    }

    public void attack(Person enemy)
    {
        enemy.getHurt(attack, armorPenetration);  //wywołuje getHurt u przeciwnika
    }

    public int getHp()
    {
        return hp;
    }

    public int getAttack()
    {
        return attack;
    }

    @Override
    public String toString()
    {
        return name;
    }

    //Metody do liczenia obrażeń
    private int damageValue(int amount, int armorPenetration)
    {
        return (amount * (100 - (calcArmorPenetration(armorPenetration)))) / 100; // zwraca wartość ataku po uwzględnieniu pancerza i penetracji
    }

    private int calcArmorPenetration(int armorPenetration) // DO POPRAWY
    {
        return (armor - (armor * armorPenetration) / 100); //oblicza o ile penetracja osłabi pancerz
    }
}

